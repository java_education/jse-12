package ru.t1.oskinea.tm.api.controller;

public interface ICommandController {

    void showErrorCommand();

    void showErrorArgument();

    void showVersion();

    void showAbout();

    void showSystemInfo();

    void showHelp();

}
