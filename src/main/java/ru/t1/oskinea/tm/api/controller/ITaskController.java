package ru.t1.oskinea.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

}
