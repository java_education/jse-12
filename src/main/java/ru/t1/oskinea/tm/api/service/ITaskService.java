package ru.t1.oskinea.tm.api.service;

import ru.t1.oskinea.tm.api.repository.ITaskRepository;
import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.model.Project;
import ru.t1.oskinea.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    public Task changeTaskStatusById(String id, Status status);

    public Task changeTaskStatusByIndex(Integer index, Status status);

    public Task updateById(String id, String name, String description);

    public Task updateByIndex(Integer index, String name, String description);

}
