package ru.t1.oskinea.tm.repository;

import ru.t1.oskinea.tm.api.repository.ITaskRepository;
import ru.t1.oskinea.tm.model.Project;
import ru.t1.oskinea.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index < 0 || index >= tasks.size()) return null;
        return tasks.get(index);
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        return remove(task);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        return remove(task);
    }

    @Override
    public int getSize() {
        return tasks.size();
    }

}
